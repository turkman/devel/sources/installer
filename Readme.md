# Turkman linux installer

## How to use
1. install dependencies:
* dialog
* cfdisk (from util-linux)
* parted
* e2fsprogs

2. remove /sbin/init and replace

## Notes:
* This installer destroy systemd based distributions. Please do not use if systemd exists.
* **/etc/sysconf.d/installer** is post install hook. It will run before grub installation.


