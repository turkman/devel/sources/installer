#!/bin/bash
set -e
trap '' 2
if [[ $$ -eq 1 ]] ; then
    mount -o remount,rw / || true
fi
export PATH=/usr/sbin:/usr/bin:/sbin:/bin
setup_parts=()
setup_grub=""
auto_disk=""
> /tmp/installer-parts
> /tmp/fstab

# cttyhack
if [[ ! -f /tmp/.ctty ]] ; then
    touch /tmp/.ctty
    exec /bin/busybox setsid \
    /sbin/init </dev/console >/dev/console 2>&1
fi

###### find partition by mount point ######
find_part(){
    cat /tmp/installer-parts | grep -e "^$1 " | cut -f1 -d" "
}

find_type(){
    blkid | grep "$1" | sed "s/.*TYPE=\"//g;s/\".*//g"
}

###### Language menu ######
lang_menu(){
    ls /usr/share/i18n/locales | grep -v "^ku" | grep -v "^hy" | grep -v "@" | while read line ; do
        lang=$(echo $line | cut -f1 -d"_")
        if [[ ! -d /usr/share/locale/$lang ]] ; then
            continue
        fi
        desc=$(cat /usr/share/i18n/locales/$line | grep "^title " | cut -f2 -d"\"" | sed "s/ /_/g")
        if [[ "$desc" == "" ]] ; then
            desc="$line"
        fi
        echo "$line $desc"
    done
}

select_language(){
    # ignore if non-glibc or locales not available
    if [[ ! -d /usr/share/i18n/locales ]] ; then
        return
    fi
    result=$(dialog --no-cancel \
                --output-fd 1 --menu \
                "$TITLE" 0 0 0 \
                $(lang_menu)
            )
    echo $result
}


###### Timezone menu ######
timezone_menu(){
    i=0
    ls -d /usr/share/zoneinfo/{*,*/*} | grep "/[A-Z]" | sort | while read zone ; do
        if [[ ! -f $zone ]] ; then
            continue
        fi
        zone=${zone/\/usr\/share\/zoneinfo\//}
        i=$(($i+1))
        echo "$zone $i"
    done
}


select_timezone(){
    if [[ ! -d /usr/share/zoneinfo ]] ; then
        return
    fi
    result=$(dialog --no-cancel \
                --output-fd 1 --menu \
                "$TITLE" 0 0 0 \
                $(timezone_menu)
            )
    echo $result
}


###### install function ######
install_main() {
    echo ${setup_grub}
    echo ${setup_parts[@]}
    # clear screen
    echo -e "\033c"
    # mount target
    mkdir -p /target
    for disk in $(cat /tmp/installer-parts | cut -f1 -d" " | sort -V) ; do
        target=$(cat /tmp/installer-parts | grep -e "^$disk " | cut -f2 -d" ")
        mkdir -p /target/$target
        while umount /target/$target ; do true ; done
        mount -t $(find_type $disk) $(find_part $disk) /target/$target
        # generate fstab
        echo $(find_part $disk) $target $(find_type $disk) defaults,rw 0 0 >> /tmp/fstab
    done
    install_common
}

install_common(){
    # mount source
    mkdir -p /source
    while umount /source ; do true ; done
    mount -o loop,ro /dev/loop0 /source
    # copy filesystem
    if command -v rsync ; then
        rsync --archive --info=progress2  \
            --no-inc-recursive --human-readable /source/ /target/
    else
    cp -prfv /source/* /target/
    fi
    cp -pfv /tmp/fstab /target/etc/fstab
    echo "proc /proc proc defaults,hidepid=2,gid=31 0 0" >> /target/etc/fstab
    # fix permissions if wrong
    chmod 1777 /target/tmp
    chmod 700 /target/data/user/root
    sync
    # mount binds
    for dir in dev sys proc ; do
        mount --bind /$dir /target/$dir
    done
    # post install hooks
    if [[ -f /target/etc/sysconf.d/installer ]] ; then
        bash /target/etc/sysconf.d/installer || true
        rm -f /target/etc/sysconf.d/installer
    fi
    for kernel in $(ls /target/lib/modules/) ; do
        chroot /target mkinitrd -u -k $kernel
    done
    # umount binds
    if [[ -d /target/sys/firmware/efi ]] ; then
        mount -t efivarfs efivarfs /target/sys/firmware/efi/efivars/
    fi
    # grub install
    chroot /target grub-install ${setup_grub}
    sed -i ".*GRUB_DISABLE_LINUX_UUID.*/d" /target/etc/default/grub
    sed -i "GRUB_DISABLE_LINUX_UUID=false" >> /target/etc/default/grub
    chroot /target grub-mkconfig -o /boot/grub/grub.cfg
    # sync disk
    echo s > /proc/sysrq-trigger
    if [[ -d /sys/firmware/efi ]] ; then
        umount -lf /target/sys/firmware/efi/efivars/
    fi
    for dir in dev sys proc ; do
        umount -lf /target/$dir
    done
    if [[ $$ -eq 1 ]] ; then
        # reboot
        echo _reisub > /proc/sysrq-trigger
    fi
}

auto_install_main(){
    # erase mbr
    dd if=/dev/zero of=${auto_disk} bs=512 count=1
    pfx=""
    if echo ${auto_disk} | grep nvme >/dev/null; then
        pfx="p"
    fi
    # create new partitions and mount
    mkdir -p /target/
    if [[ -d /sys/firmware/efi ]] ; then
        yes | parted ${auto_disk} mktable gpt
        yes | parted ${auto_disk} mkpart primary fat32 1 "500MB"
        yes | parted ${auto_disk} mkpart primary fat32 500MB "100%"
        yes | mkfs.vfat ${auto_disk}${pfx}1
        yes | mkfs.ext4 -O ^has_journal ${auto_disk}${pfx}2
        mount -t ext4 ${auto_disk}${pfx}2 /target
        mkdir -p /target/boot/efi
        mount -t vfat ${auto_disk}${pfx}1 /target/boot/efi
        echo ${auto_disk}${pfx}2 / ext4 defaults,rw 0 0 >> /tmp/fstab
    else
        yes | parted ${auto_disk} mktable msdos
        yes | parted ${auto_disk} mkpart primary fat32 1 "100%"
        yes | mkfs.ext4 -O ^has_journal ${auto_disk}${pfx}1
        mount -t ext4 ${auto_disk}${pfx}1 /target
        echo ${auto_disk}${pfx}1 / ext4 defaults,rw 0 0 >> /tmp/fstab
    fi
    sync
    install_common
}

###### Main Menu ######
main_menu(){
    while true ; do
        result=$(dialog --no-cancel \
            --output-fd 1 --menu \
            "Main Menu" 0 0 0 \
            "1" "Automated Install" \
            "2" "Manual Install" \
            "3" "Start in Live Mode" \
            "4" "Run Terminal" \
            "0" "Reboot"
        )
        echo -ne "\033c"
        if [[ $result -eq 0 ]] ; then
            reboot -f
        elif [[ $result -eq 1 ]] ; then
            auto_install_menu
        elif [[ $result -eq 2 ]] ; then
            install_menu
        elif [[ $result -eq 3 ]] ; then
            start_openrc
        elif [[ $result -eq 4 ]] ; then
           echo -e "\033[33;1mTerminal mode. You can type exit and go back.\033[;0m"
            PS1="\033[32;1m>>>\033[;0m " /bin/busybox ash
        fi
        echo $result
    done
}

install_menu(){
    while true ;  do
        result=$(dialog --no-cancel \
            --output-fd 1 --menu \
            "Install menu" 0 0 0 \
            "e" "Erase Partition" \
            "1" "Create / Modify Partition" \
            "2" "Select partition" \
            "${setup_parts[@]}" \
            "i" "Install now" \
            "0" "Go back"
        )
        if [[ "$result" == "e" ]] ; then
           erase_partition=/dev/$(TITLE="Select a disk for erase" select_disk)
           head -c 512 /dev/zero > ${erase_partition}
           sync
           continue;
        elif [[ "$result" == "i" ]] ; then
           setup_grub=/dev/$(TITLE="Select a disk for grub bootloader" select_disk)
           install_main
           break
        elif [[ $result -eq 1 ]] ; then
            cfdisk /dev/$(TITLE="Select a disk for edit" select_disk)
        elif [[ $result -eq 2 ]] ; then
            partition=/dev/$(TITLE="Select a partition for mountpoint" select_partition)
            mountpoint=$(mountpoint_menu)
            invalid=0
            for arg in "${setup_parts[@]}" ; do
                if echo "$arg" | grep '^'"$partition"'$' ; then
                    invalid=1
                fi
                if echo "$arg" | grep '^'"$mountpoint"'$' ; then
                    invalid=1
                fi
                if [[ ! -b "$partition" ]] ; then
                    invalid=1
                fi
            done
            if [[ "$invalid" -eq 1 ]] ; then
                dialog --output-fd 1 --msgbox "Please select another partition or mountpoint" 0 0
                continue
            else
                setup_parts+=($partition $mountpoint)
                echo -e "$partition $mountpoint" >> /tmp/installer-parts
            fi
            format_type=$(dialog --no-cancel \
                --output-fd 1 --menu \
                "Select filesystem format for $partition" 0 0 0 \
                "none" "do not format" \
                "ext2" "filesystem for legacy linux rootfs" \
                "ext4" "filesystem for linux rootfs partition or storage" \
                "fat32" "filesystem for removable devices of efi partition")
            if [[ ${format_type} == "ext2" ]] ; then
                echo -ne "\033c"
                yes | mkfs.ext2 $partition
            elif [[ ${format_type} == "ext4" ]] ; then
                echo -ne "\033c"
                yes | mkfs.ext4 -O ^has_journal -O $partition
            elif [[ ${format_type} == "fat32" ]] ; then
                echo -ne "\033c"
                yes | mkfs.vfat $partition
            fi
        elif [[ $result -eq 0 ]] ; then
            > /tmp/installer-parts
            break
        else
            partition=$result
            mountpoint=$(grep /tmp/installer-parts -e "^$result" | cut -f2 -d" ")
            setup_parts=(${setup_parts[@]/"$partition"})
            setup_parts=(${setup_parts[@]/"$mountpoint"})
            sed -i "s|^$partition .*||g;/^$/d" /tmp/installer-parts
        fi
    done
}

auto_install_menu(){
    while true ;  do
       auto_disk=/dev/$(TITLE="Select a disk for installation" select_disk)
       setup_grub=${auto_disk}
       auto_install_main
    done
}

mountpoint_menu(){
    if [[ -d /sys/firmware/efi ]] ; then
        efi_mounts=("/boot/efi" "Boot directory for uefi")
    fi
    result=$(dialog --no-cancel \
                --output-fd 1 --menu \
                "Select a mountpoint" 0 0 0 \
                "/" "Root filesystem" \
                "${efi_mounts[@]}" \
                "custom" "Custom directory")
    if [[ "$result" == "custom" ]] ; then
        result=""
        while ! mkdir -p "$result" ; do
            result=$(dialog --output-fd 1 --inputbox "Enter new mountpoint" 0 0)
        done
    fi
    echo $result
}


###### Partition selection menu ######
select_partition(){
    export PATH="/bin:/sbin:/usr/bin:/usr/sbin"
    menu=()
    for disk in /sys/block/* ; do
        disk=${disk/*\//}
        if echo $disk | grep "^loop" >/dev/null ; then
            continue
        fi
        if echo $disk | grep "^zram" >/dev/null ; then
            continue
        fi
        for part in /sys/block/$disk/$disk* ; do
            if [[ -d $part ]] ; then
                part=${part/*\//}
                type=$(blkid | grep "/dev/$part:" | sed "s/.* TYPE=\"//g;s/\".*//g")
                label=$(blkid | grep "/dev/$part:" | grep LABEL | sed "s/.* LABEL=\"//g;s/\".*//g")
                if [[ "$label" == "" ]] ; then
                    label="-"
                fi
                size=$(lsblk -r | grep "^$part " | cut -f4 -d" ")
                menu+=("$part" "$type    $label    $size")
            fi
        done
    done >/dev/null
    info_label="Partition | Filesystem | Label | Size"
    while [[ ! -b "/dev/$result" ]] ; do
        result=$(dialog --no-cancel \
            --output-fd 1 --menu \
            "$TITLE\n\n${info_label}" 0 0 0 \
            "${menu[@]}")
    done
    echo $result
}

###### Disk selection menu ######
select_disk(){
    export PATH="/bin:/sbin:/usr/bin:/usr/sbin"
    menu=()
    for disk in /sys/block/* ; do
        disk=${disk/*\//}
        if echo $disk | grep "^loop" >/dev/null ; then
            continue
        fi
        if [[ "$(cat /sys/block/$disk/size)" -eq 0 ]] ; then
            continue
        fi
        size=$(lsblk -r | grep "^$disk " | cut -f4 -d" ")
        name=$(cat /sys/block/$disk/device/model)
        menu+=("$disk" " $name ($size)")
    done >/dev/null
    while [[ ! -b "/dev/$result" ]] ; do
        result=$(dialog --no-cancel \
            --output-fd 1 --menu \
            "$TITLE" 0 0 0 \
            "${menu[@]}")
    done
    echo $result
}

###### username password menu ######
get_password(){
    while [[ "$pass" == "" ]] ; do
       pass=$(dialog  --no-cancel --output-fd 1 \
            --title "$TITLE" \
            --backtitle "initial settings" \
            --inputbox "Enter password" 0 0
        )
    done
    echo $pass
}
get_username(){
    while [[ "$user" == "" ]] ; do
       user=$(dialog  --no-cancel --output-fd 1 \
            --title "$TITLE" \
            --backtitle "initial settings" \
            --inputbox "Enter username" 0 0
        )
    done
    echo $user
}

###### init replace functions ######

start_openrc(){
    trap 2
    exec /sbin/openrc-init

}

fix_init(){
    # replace init with openrc
    rm -f /sbin/init
    ln -s openrc-init /sbin/init
}

###### first setup menu ######
first_setup(){
    while true ; do
        result=$(dialog --no-cancel \
            --output-fd 1 --menu \
            "First Setup" 0 0 0 \
            1 "Use default settings" \
            2 "Use custom settings" \
            3 "Run terminal" \
            0 "Reboot")
        if [[ "$result" == "0" ]] ; then
            reboot -f
        elif [[ "$result" == "1" ]] ; then
            useradd -m pingu -s /bin/bash
            for grp in audio video input wheel users proc; do
                usermod -aG $grp pingu || true
            done
            r=$(TITLE="Please provide a password" get_password)
            echo -e "$r\n$r\n" | passwd pingu
            echo -e "$r\n$r\n" | passwd root
            usermod -aG proc polkitd || true
            rm -f /etc/localtime || true
            ln -s ../usr/share/zoneinfo/UTC /etc/localtime
            fix_init
            start_openrc
        elif [[ "$result" == "2" ]] ; then
            u=$(TITLE="Please provide an username" get_username)
            r=$(TITLE="Please provide a password" get_password)
            l=$(TITLE="Please select language" select_language)
            t=$(TITLE="Please select timezone" select_timezone)
            useradd -m "$u" -s /bin/bash
            for grp in audio video input wheel users proc; do
                usermod -aG $grp "$u" || true
            done
            usermod -aG proc polkitd || true
            echo -e "$r\n$r\n" | passwd "$u"
            echo -e "$r\n$r\n" | passwd root
            if [[ "$l" != "" ]] ; then
                mkdir -p /lib64/locale
                echo "$l.UTF-8 UTF-8" > /etc/locale.gen
                echo "" >> /etc/locale.gen
                echo "export LANG=$l.UTF-8" > /etc/profile.d/locale.sh
                echo "export LC_ALL=$l.UTF-8" >> /etc/profile.d/locale.sh
                locale-gen
            fi
            if [[ "$t" != "" ]] ; then
                rm -f /etc/localtime || true
                ln -s ../usr/share/zoneinfo/"$t" /etc/localtime
            fi
            fix_init
            start_openrc
        elif [[ "$result" == "3" ]] ; then
            create_shell
        fi
    done
}


###### terminal ######
create_shell(){
    set +e
    echo -e "\033[33;1mTerminal mode. You can type exit and go back.\033[;0m"
    trap 2
    PS1="\033[32;1m>>>\033[;0m " \
        /bin/busybox ash
    trap '' 2
    set -e
}

###### begin script ######

if [[ $$ -eq 1 ]] ; then
    depmod -a
    mkdir -p /run/udev
    udevd --daemon
    udevadm trigger --action=add
    udevadm settle
    udevadm control --exit
fi

while true ; do
    if grep "boot=live" "/proc/cmdline"; then
        main_menu || create_shell
    else
        first_setup || create_shell
    fi
done
